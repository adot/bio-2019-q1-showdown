{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Redundant bracket" #-}
module Main where

import Debug.Trace (trace)

base = 10

baseF = fromIntegral base

digCount :: Integer -> Integer
digCount x = (truncate (logBase baseF x')) + 1
  where
    x' = fromIntegral x :: Float

integerExtract :: Integer -> Integer -> Integer -> Integer -- num, start Index from right, end index from right (0 is funky)
integerExtract n i j = integerExtract' n' (i - j) 0
  where
    n' = div n (base ^ j)
    integerExtract' :: Integer -> Integer -> Integer -> Integer
    integerExtract' _ 0 _ = 0
    integerExtract' n'' x b = (n'' `mod` base) * (base ^ b) + integerExtract' (div n'' base) (x - 1) (b + 1)

digReverse :: Integer -> Integer
digReverse = flip digReverse' 0
  where
    digReverse' :: Integer -> Integer -> Integer
    digReverse' 0 acc = acc
    digReverse' x acc = digReverse' (div x base) ((acc * base) + (x `mod` base))

palinJacob :: Integer -> Integer
palinJacob x =
  trace
    ( "x = "
        ++ show x
        ++ "; l = "
        ++ show l
        ++ "; hl ="
        ++ show hl
        ++ "; fh = "
        ++ show fh
        ++ "; fhp = "
        ++ show fhp
        ++ "; fh' = "
        ++ show fh'
        ++ "; fhp' = "
        ++ show fhp'
        ++ "; ans = "
        ++ show ans
    )
    ans
  where
    l = digCount x
    hl = div l 2
    fh = findFH x l
    fhp = findFHP x l
    fh' = digReverse fh
    fhp' = fhp + 1
    ans = if (fh' > findSH x l) then (fhp * (base ^ hl)) + fh' else palinJacobInc x
    palinJacobInc x
      | even l = fhp' * (base ^ hl) + (digReverse (fhp'))
      | otherwise = fhp' * (base ^ hl) + (digReverse (div fhp' base))

findFH :: Integer -> Integer -> Integer
findFH x l = integerExtract x l (l - (div l 2))

findSH :: Integer -> Integer -> Integer
findSH x l = integerExtract x (l - (div l 2) - (mod l 2)) 0

findFHP :: Integer -> Integer -> Integer
findFHP x l
  | even l = findFH x l
  | otherwise = ((findFH x l) * base) + (integerExtract x (((div l 2)) + 1) (div l 2))

-- cases :: [(Integer, Integer)]
cases =
  [ (4, 5),
    (5, 6),
    (9, 11),
    (33, 44),
    (84, 88),
    (45653, 45654),
    (36460000, 36466463),
    (24355343, 24366342),
    (123450000, 123454321),
    (234567890, 234575432),
    (678999876, 679000976),
    (99999999999999, 100000000000001),
    (999999999999999, 1000000000000001),
    (123456789000000000, 123456789987654321),
    (987654321123456789, 987654322223456789),
    (1234567890000000000, 1234567890987654321),
    (9876543210123456789, 9876543211123456789),
    (9876543219123456789, 9876543220223456789)
  ]

bigCases = take 300000 (cycle cases)

isRight (k, v) = palinJacob k == v

correct = map isRight bigCases

allCorrect = and correct

main = print allCorrect
