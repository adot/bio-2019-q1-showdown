const BASE: u64 = 10;
const BASE_F: f32 = BASE as f32;

fn dig_count(x: u64) -> u64 {
    (x as f32).log(BASE_F) as u64 + 1
}

fn integer_extract(n: u64, i: u64, j: u64) -> u64 {
    fn inner(n: u64, x: u64, b: u64) -> u64 {
        if x == 0 {
            0
        } else {
            (n % BASE) * BASE.pow(b as _) + inner(n / BASE, x - 1, b + 1)
        }
    }
    let nprim = n / BASE.pow(j as _);
    inner(nprim, i - j, 0)
}

fn dig_reverse(n: u64) -> u64 {
    fn inner(x: u64, acc: u64) -> u64 {
        if x == 0 {
            acc
        } else {
            inner(x / BASE, acc * BASE + x % BASE)
        }
    }

    return inner(n, 0);
}

fn find_fh(x: u64, l: u64) -> u64 {
    integer_extract(x, l, l - l / 2)
}
fn find_sh(x: u64, l: u64) -> u64 {
    integer_extract(x, l - l / 2 - l % 2, 0)
}
fn find_fhp(x: u64, l: u64) -> u64 {
    if l % 2 == 0 {
        find_fh(x, l)
    } else {
        (find_fh(x, l) * BASE) + (integer_extract(x, l / 2 + 1, l / 2))
    }
}

fn palinJacob(x: u64) -> u64 {
    let l = dig_count(x);
    let hl = l / 2;
    let fh = find_fh(x, l);
    let fhp = find_fhp(x, l);

    let fh_ = dig_reverse(fh);
    let fhp_ = fhp + 1;

    let pji = |x| {
        if x % 2 == 0 {
            fhp_ * (BASE.pow(hl as _)) + dig_reverse(fhp_)
        } else {
            fhp_ * (BASE.pow(hl as _)) + dig_reverse(fhp_ / BASE)
        }
    };

    if fh_ > find_sh(x, l) {
        fhp * (BASE.pow(hl as _)) + fh_
    } else {
        pji(l)
    }
}

fn main() {
    let next_largext_pal = palinJacob;

    let testcases = [
        (5, 6),
        (9, 11),
        (33, 44),
        (84, 88),
        (45653, 45654),
        (36460000, 36466463),
        (24355343, 24366342),
        (123450000, 123454321),
        (234567890, 234575432),
        (678999876, 679000976),
        (99999999999999, 100000000000001),
        (999999999999999, 1000000000000001),
        (123456789000000000, 123456789987654321),
        (987654321123456789, 987654322223456789),
        (1234567890000000000, 1234567890987654321),
        (9876543210123456789, 9876543211123456789),
        (9876543219123456789, 9876543220223456789),
    ];

    const NTEST: usize = 1000000;
    let mut big_arr = Vec::with_capacity(NTEST);
    while big_arr.len() < NTEST {
        big_arr.extend_from_slice(&testcases);
    }

    for (m, n) in big_arr {
        let r = next_largext_pal(m);
        assert_eq!(r, n, "next_largest_pal({m}) = {r}, but wanted {n}");
    }
}
