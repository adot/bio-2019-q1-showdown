def next_largest_pal(t):
    n, m = [int(i) for i in str(t)], len(str(t)) // 2
    for i in range(m):
        n[len(n) - i - 1] = n[i]
    a = int("".join(str(i) for i in n))
    if a > t:
        return a
    lo = m - (0 if len(n) % 2 else 1)
    while m < len(n):
        if n[lo] != 9:
            n[lo], n[m] = n[lo] + 1, n[m] + 1
            return int("".join(str(i) for i in n))
        n[lo], n[m], lo, m = 0, 0, lo - 1, m + 1
    return int("1" + "0" * (len(n) - 1) + "1")


testcases = [
    (5, 6),
    (9, 11),
    (33, 44),
    (84, 88),
    (45653, 45654),
    (36460000, 36466463),
    (24355343, 24366342),
    (123450000, 123454321),
    (234567890, 234575432),
    (678999876, 679000976),
    (99999999999999, 100000000000001),
    (999999999999999, 1000000000000001),
    (123456789000000000, 123456789987654321),
    (987654321123456789, 987654322223456789),
    (1234567890000000000, 1234567890987654321),
    (9876543210123456789, 9876543211123456789),
    (9876543219123456789, 9876543220223456789),
]

NTEST = 300000

bigArr = []
while len(bigArr) < NTEST:
    bigArr.extend(testcases)

bigArr = bigArr[:NTEST]
for a, b in bigArr:
    assert next_largest_pal(a) == b
