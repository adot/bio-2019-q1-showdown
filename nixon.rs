fn next_largext_pal(t: u64) -> u64 {
    // https://crates.io/crates/itoa
    let mut buffer = itoa::Buffer::new();
    let n = buffer.format(t).as_bytes();
    let mut n_mut: [u8; 19] = std::array::from_fn(|_| 0);
    for (idx, i) in n.iter().enumerate() {
        n_mut[idx] = *i - b'0';
    }
    let l = n.len();
    let n = &mut n_mut[..l];
    let mut m = l / 2;
    for i in 0..m {
        n[l - i - 1] = n[i];
    }
    let a = n.iter().fold(0, |acc, x| acc * 10 + *x as u64);
    if a > t {
        return a;
    }
    let mut lo = m - if l % 2 == 0 { 1 } else { 0 };
    while m < n.len() {
        if n[lo] != 9 {
            let (nlo, nhi) = (n[lo] + 1, n[m] + 1);
            n[lo] = nlo;
            n[m] = nhi;
            let a = n.iter().fold(0, |acc, x| acc * 10 + *x as u64);
            return a;
        }
        n[lo] = 0;
        n[m] = 0;
        lo = lo.wrapping_sub(1);
        m += 1;
    }
    return format!("1{}1", "0".repeat(l - 1)).parse().unwrap();
}

fn main() {
    let testcases = [
        (5, 6),
        (9, 11),
        (33, 44),
        (84, 88),
        (45653, 45654),
        (36460000, 36466463),
        (24355343, 24366342),
        (123450000, 123454321),
        (234567890, 234575432),
        (678999876, 679000976),
        (99999999999999, 100000000000001),
        (999999999999999, 1000000000000001),
        (123456789000000000, 123456789987654321),
        (987654321123456789, 987654322223456789),
        (1234567890000000000, 1234567890987654321),
        (9876543210123456789, 9876543211123456789),
        (9876543219123456789, 9876543220223456789),
    ];

    const NTEST: usize = 1000000;
    let mut big_arr = Vec::with_capacity(NTEST);
    while big_arr.len() < NTEST {
        big_arr.extend_from_slice(&testcases);
    }

    for (m, n) in big_arr {
        let r = next_largext_pal(m);
        assert_eq!(r, n, "next_largest_pal({m}) = {r}, but wanted {n}");
    }
}
